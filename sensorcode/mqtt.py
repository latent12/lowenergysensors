last_message = 0
message_interval = 10

class MQTT:

    @staticmethod
    def connect_mqtt():
       global client_id, mqtt_server
       client = MQTTClient(client_id, mqtt_server)
       #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
       client.connect()
       print('Connected to %s MQTT broker' % (mqtt_server))
       return client
    @staticmethod
    def restart_and_reconnect():
       print('Failed to connect to MQTT broker. Reconnecting...')
       time.sleep(10)
       machine.reset()  




