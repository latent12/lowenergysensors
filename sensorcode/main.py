import mqtt
import collector
import time

try:
  client = MQTT.connect_mqtt()
except OSError as e:
  MQTT.restart_and_reconnect()

while True:
  
  last_message = 0
  message_interval = 10
  
  if (time.time() - last_message) > message_interval:
    
    try:
    
       time.sleep(10)
       Collector.collect_sensor()
       Collector.publish_sensor()
       time.sleep(10)
       
    except OSError as e:
		     print("Cant read:", e)
		     sys.print_exception(e)
    
    last_message = time.time()









