from machine import Pin
import motionconnect
import time
from time import sleep

class MotionPresence():
  
    value_type = bool
    
    motion = MotionConnect.motion_config()
    
    motion_triggered = 0
    motion_triggered_since_last_reading = 0
     
    def handleEdge(self):
      
       results = self.motion
       self.motion_triggered = results.value()
       
       if self.motion_triggered != 0:
         #print ('Motion Detected In Room')
         self.motion_triggered = 1
         self.motion_triggered_since_last_reading = 1
       else:
           #print ('No Motion Detected In Room')
           self.motion_triggered = 0
           
    #Collect Sensor Values
    def get_sensor_value(self):
      
        self.handleEdge()
        self.motion_triggered_since_last_reading = self.motion_triggered
        value = self.motion_triggered_since_last_reading
        
        return (value)
        
        #return "{:.2f}".format(value)
        
        #return "Motion = {:.2f}".format(value)


class MotionSensor():
  
    value_type = bool
    
    motion = MotionConnect.motion_config()
    
    motion_triggered = 0
    motion_triggered_since_last_reading = 0
     
    def handleEdge(self):
      
       results = self.motion
       self.motion_triggered = results.value()
       
       if self.motion_triggered != 0:
         print ('Active')
         self.motion_triggered = 1
         self.motion_triggered_since_last_reading = 1
       else:
           print ('Inactive')
           self.motion_triggered = 0
           
    #Collect Sensor Values
    def get_sensor_value(self):
      
        self.handleEdge()
        self.motion_triggered_since_last_reading = self.motion_triggered
        value = self.motion_triggered_since_last_reading
        
        return "Motion = {:.2f}".format(value)

#while True:
  
  #time.sleep(5)
  #pir = MotionSensor()
  #print(pir.value())
  #print (pir.get_sensor_value())
  #time.sleep(5)











