from machine import Pin, I2C
import lightconnect
import time

_DEVICE_ADDR = 0x23 # default I2C address for BH1750 light sensor

class LightSensor():
  
  #Light Modes
  OP_SINGLE_HRES1 = 0x20
  OP_SINGLE_HRES2 = 0x21
  OP_SINGLE_LRES = 0x23

  DELAY_HMODE = 180  # 180ms in H-mode
  DELAY_LMODE = 24  # 24ms in L-mode
  
  LightConnect.light_variables()

  mode = None
  
  def get_sensor_value(self): 
    
    if self.i2c != None:
      
      LightConnect.light_values(self)
      results = LightConnect.light_results(self)
      
      if results != None and len(results) == self.i2c_transaction_length:
        ch0 = results[0]
        ch1 = results[1]
        #return "Light = {:.2f}".format(self._data_to_lux(ch0, ch1))
        return self._data_to_lux(ch0, ch1)
        
    else:
      print ("Problem with response from i2c service: %s" % results)
      return None
    
  def _power_up(self):
    
    LightConnect.light_power(self) 
    
  def set_mode(self, mode):
    self.mode = mode
    LightConnect.light_mode(self, mode)
    time.sleep_ms(self.DELAY_LMODE if mode == self.OP_SINGLE_LRES else self.DELAY_HMODE)
       
  def _data_to_lux(self, ch0, ch1):
    lux = 0
    if (ch0 == 0):
      return lux
    factor = 2.0 if self.mode==self.OP_SINGLE_HRES2 else 1.0
    lux = (ch0<<8 | ch1) / (1.2 * factor)
    return lux

  def __init__(self):
    self.i2c = I2C(0, scl=Pin(22), sda=Pin(21))
    self.i2c_address = _DEVICE_ADDR
    self.i2c_transaction_length = 2
    
#def main():
  #while True:
    #time.sleep(5)
    #sensor = LightSensor()
    #print ('Light = ', sensor.get_sensor_value())

#if (__name__ == "__main__"):
  #main()








