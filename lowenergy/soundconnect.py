class SoundConnect:
  
  @staticmethod
  def sound_connect():
  
     sound = machine.ADC(Pin(36))
     return sound.read()

