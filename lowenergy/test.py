
from machine import Pin, TouchPad, lightsleep
import mqtt
import esp32
import collector
import time

try:
  client = MQTT.connect_mqtt()
except OSError as e:
  MQTT.restart_and_reconnect()

def sub_cb(topic, msg):
  
      global header
      header = topic.decode('utf-8')
      
      global message
      message = msg.decode('utf-8')
      
      sub_variables()
      
def sub_variables():
  
    if header == L1Topic:
       convertL1 = int(message)
       global L1
       L1 = convertL1
      
    if header == L2Topic: #L2 Topic
       convertL2 = int(message)
       global L2
       L2 = convertL2
      
    if header == L3Topic: #L3 Topic
       convertL3 = int(message)
       global L3
       L3 = convertL3
      
    if header == L4Topic: #L4 Topic
       convertL4 = int(message)
       global L4
       L4 = convertL4
    
    client.check_msg()
    
    return (L1, L2, L3, L4)
      
client.set_callback(sub_cb)
client.connect()

L1Topic = client.subscribe("L1Timer", qos=1)
print(L1Topic)
#L2Topic = client.subscribe("L2Timer", qos=2)
#L3Topic = client.subscribe("L3Timer", qos=2)
#L4Topic = client.subscribe("L4Timer", qos=2)

while True:
  
  client.check_msg()
  
  LValues = [L1, L2, L3, L4]
  LValues.append(sub_variables())
    
  for LValue in LValues:
      print(LValues)

