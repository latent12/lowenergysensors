
import machine
from machine import Pin, ADC
import soundconnect
import time

_SOUND_SENSOR_SAMPLING_HZ_ = 20 
_SOUND_SENSOR_SAMPLING_PERIOD_ = 1

class SoundPresence():
 
 
    def __init__(self, freq = _SOUND_SENSOR_SAMPLING_HZ_ , period = _SOUND_SENSOR_SAMPLING_PERIOD_):
      
        self.sound_samples = []
        self.sample_freq = freq
        self.sample_period = period

    def get_sensor_value(self):
      
        try:
            sleep_time = (1.0/self.sample_freq)
            no_of_samples = (self.sample_period / sleep_time)
            samples = SoundConnect.sound_connect()
            
            # Collect samples at a regular interval
            for idx in range(0, int(no_of_samples) ):
                time.sleep( sleep_time )
            
            #return "Sound = {:.2f}".format(samples)
            return samples

        except Exception as e:
            print ("Error in get_sensor_value: %s" % e)
            return None

    def _median(self, samples):
        """ returns the median value of the given list. """
        v = samples[:]
        length = len(v)
        v.sort()

        if length < 1:
            return None
        elif length % 2 == 0:
             #length is even, return average of two middle items
             return (v[length / 2] + v[length / 2 - 1] ) / 2.0
        else:
             #length is odd, return the middle or only item
             return v[(length - 1) / 2]


class SoundSensor():

    def __init__(self, freq = _SOUND_SENSOR_SAMPLING_HZ_ , period = _SOUND_SENSOR_SAMPLING_PERIOD_):
      
        self.sound_samples = []
        self.sample_freq = freq
        self.sample_period = period

    def get_sensor_value(self):
      
        try:
            sleep_time = (1.0/self.sample_freq)
            no_of_samples = (self.sample_period / sleep_time)
            samples = SoundConnect.sound_connect()
            
            # Collect samples at a regular interval
            for idx in range(0, int(no_of_samples) ):
                time.sleep( sleep_time )
            
            #return "Sound = {:.2f}".format(samples)
            return samples

        except Exception as e:
            print ("Error in get_sensor_value: %s" % e)
            return None

    def _median(self, samples):
        """ returns the median value of the given list. """
        v = samples[:]
        length = len(v)
        v.sort()

        if length < 1:
            return None
        elif length % 2 == 0:
             #length is even, return average of two middle items
             return (v[length / 2] + v[length / 2 - 1] ) / 2.0
        else:
             #length is odd, return the middle or only item
             return v[(length - 1) / 2]

#while True:
  
  #time.sleep(5)
  #wave = SoundSensor()
  #print ('Sound = ', wave.get_sensor_value())
  #time.sleep(5)






