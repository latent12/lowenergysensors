from machine import Pin, TouchPad, lightsleep
import mqtt
import esp32
import collector
import time

try:
  client = MQTT.connect_mqtt()
except OSError as e:
  MQTT.restart_and_reconnect()

def sub_cb(topic, msg):
  
      global headers
      headers = [topic.decode('utf-8')]
      
      global messages
      messages = msg.decode('utf-8')
      
      sub_variables()
      
def sub_variables():
  
  if L1Topic == headers:
  
     L1 = headers[0]
     print(L1)
  
  #for header in headers:
      #print(headers)
      
client.set_callback(sub_cb)
client.connect()

L1Topic = client.subscribe("L1Timer", qos=2)
L2Topic = client.subscribe("L2Timer", qos=2)
#client.subscribe("L3Timer", qos=2)
#client.subscribe("L4Timer", qos=2)

while True:
  
  client.check_msg()
