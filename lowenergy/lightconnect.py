class LightConnect:
  
  @staticmethod
  def light_variables():
   
      i2c = None
      i2c_address = 0
      i2c_transaction_length = 0
      return i2c, i2c_address, i2c_transaction_length
      
  @staticmethod
  def light_values(self):
   
      self.i2c.writeto(self.i2c_address, b"\x00") # power off
      self._power_up()
      self.set_mode(self.OP_SINGLE_HRES2)  # highest accuracy
  
  @staticmethod  
  def light_results(self):
      
      results = self.i2c.readfrom(self.i2c_address, self.i2c_transaction_length)
      return results
      
  @staticmethod
  def light_power(self):
    
      self.i2c.writeto(self.i2c_address, b"\x01")  # power on
      self.i2c.writeto(self.i2c_address, b"\x07")  # reset   
    
  @staticmethod
  def light_mode(self, mode):
    
      self.i2c.writeto(self.i2c_address, bytes([self.mode]))  # set measurement mode


