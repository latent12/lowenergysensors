#PWLE Has To WakeUp From Sleep And Collect Data At Different Intervals Depending On Occupancy Status
#Application Utilises Temporal Occupation Status Methodology (TOSM)

#Scheduler

  #L0:  Generic Timer
  #L1:  Occupied Room Light, Motion and Sound in: default 1 minutes [1]
  #L2:  Unoccupied Room Air Quality, Humidity & Temperature: default 15 minutes[3]
  #L3:  Occupied: Air Quality, Humidity & Temperature: default 5 minutes[6]
  #L4:  Period to wait to determine room is unoccupied: default 5 minutes[3?]
  
#There Are Two Categories Of Sensors:
  #1 - Occupation Detector Sensors (ODS) - Motion, Sound
  #2 - Data Collector Sensors (DCS) - Temp, Light, Air

from machine import Pin, TouchPad, lightsleep
import mqtt
import esp32
import collector
import time

try:
  client = MQTT.connect_mqtt()
except OSError as e:
  MQTT.restart_and_reconnect()
  
def mqttMessages(topic, msg):
     
     global header
     header = topic.decode('utf-8') #Topic Being Sent As String Via MQTT
     
     global message
     message = msg.decode('utf-8') #Values Being Sent As String Via MQTT
     
     variableParameters()
     
def variableParameters():
  
    if header == 'L1Timer':
       convertL1 = int(message)
       global L1
       L1 = convertL1
       client.check_msg()
       return L1
      
    if header == 'L2Timer': #L2 Topic
       convertL2 = int(message)
       global L2
       L2 = convertL2
       client.check_msg()
       return L2
      
    if header == 'L3Timer': #L3 Topic
       convertL3 = int(message)
       global L3
       L3 = convertL3
       client.check_msg()
       return L3
      
    if header == 'L4Timer': #L4 Topic
       convertL4 = int(message)
       global L4
       L4 = convertL4
       client.check_msg()
       return L4
      
    #client.check_msg()
    
    #return (L1, L2, L3, L4)
   
def occupiedRoomEvent():
     
     #Lvalues = [L1, L3]
     #Lvalues.append(variableParameters())
  
     #Debug Check
     #if L1 == 6:
     #print('L1 Has Been Converted From String To Integer \n')
      
     global lastOccupiedEvent
     lastOccupiedEvent = time.time()
     currentEventTime = time.time()
     secSinceCollectData = 0
     
     while True:
       
        #client.check_msg()
       
        LValues = []
        LValues.append(variableParameters())
       
        print('The Value Of L1 Is ', L1, '\n')
       
        #time.sleep(5)
        
        #client.check_msg()
     
        #Debug Check
        print('currentEventTime = ',currentEventTime, '\n')
        print('lastOccupiedEvent = ', lastOccupiedEvent, '\n')
     
        sleepTime = L1 - (currentEventTime - lastOccupiedEvent)
     
        #Debug Check
        print('\n')
        print('sleepTime = ', sleepTime, '\n')
     
        #Determines If Room Is Occupied Or Unoccupied
        #If sleepTime Yields A Result > 0/Positive Value - Room Considered Occupied
        #Unoccupied Will Yield A Result <= 0/Negative Value - Room Considered Unoccupied
        if sleepTime <= 0:
           return
        
        #Sensors Must Be In Sleep Mode Then Wake Up To Take Readings
        print('Room Is Currently Occupied \n')
        print('Going Into Light Sleep Mode \n')
        print('Current Light Sleep Time Is Set To', sleepTime, 'ms \n')
  
        #An Interrupt Means That The ODS Sensors Detected Some Presence In Room
        odsInterrupt()
  
        #time.sleep(5)
     
        #Timer
        machine.lightsleep(sleepTime * 1000) #Timer Calculates In ms
  
        #Debug Check
        #If Woken By Interrupt, Value Returned Is 2
        #If Woken By Timer, Value Returned Is 4
        wakeValue = machine.wake_reason()
        print('wakeValue = ', wakeValue, '\n')
     
        if wakeValue == 2:
           print('Light Sleep Interrupted By ODS Sensors \n')
           lastOccupiedEvent = time.time()
           print('Last Occupied Event Is Now', lastOccupiedEvent, '\n')
        elif wakeValue == 4:
             print('Light Sleep Ended By Timer \n')
             
             #client.check_msg()
                  
             #if L3 == 30:
                #print('L3 Has Been Converted From String To Integer \n')
             LValues = []
             LValues.append(variableParameters())
             print('The Value Of L3 Is ', L3, '\n')
             #else:
                 #return
            
             secSinceCollectData = secSinceCollectData + L1
             print('Time Elapsed = ', secSinceCollectData, 'secs \n')
         
             if secSinceCollectData >= L3:
                collectData()
                print('\n')
                print('Data Collection For This Period Complete \n')
                
def unoccupiedRoomEvent():
  
  client.check_msg()
  
  #Debug Check
  if L4 == 42:
     print('L4 Has Been Converted From String To Integer \n')
     print('The Value Of L4 Is ', L4, '\n')
     
  currentEventTime = time.time()
  lastUnoccupiedEvent = time.time()
  secSinceCollectData = 0
  
  #Debug Check
  print('currentEventTime = ', currentEventTime, '\n')
  print('lastunoccupiedEvent = ', lastUnoccupiedEvent, '\n')
  
  while True:
    
     #time.sleep(5)
     
     print('Room Is Currently Unoccupied \n')
     
     odsInterrupt()
     
     #time.sleep(5)
  
     #Invoke Light Sleep Mode For L4 mins
     machine.lightsleep(L4 * 1000)
     
     #Debug Check
     #If Woken By Interrupt, Value Returned Is 2
     #If Woken By Timer, Value Returned Is 4
     wakeValue = machine.wake_reason()
     print('wakeValue = ', wakeValue, '\n')
  
     if wakeValue == 2:
        print('Light Sleep Interrupted By ODS Sensors \n')
        lastOccupiedEvent = time.time()
        print('Last Occupied Event Is Now', lastOccupiedEvent, '\n')
        return
     elif wakeValue == 4:
          print('Light Sleep Ended By Timer \n')
          print('Room Is Still Unoccupied \n')
          
          if L2 == 90:
                print('L2 Has Been Converted From String To Integer \n')
                print('The Value Of L2 Is ', L2, '\n')
          else:
             return
            
             secSinceCollectData = secSinceCollectData + L4
             print('Time Elapsed = ', secSinceCollectData, 'secs \n')
         
             if secSinceCollectData >= L4:
                collectData()
                print('\n')
                print('Data Collection For This Period Complete \n')
            
def odsInterrupt():
  
  #Interrupt Due To Motion Sensor
  wake = Pin(0, mode = Pin.IN, pull = Pin.PULL_DOWN)
  esp32.wake_on_ext0(pin = wake, level = Pin.WAKE_HIGH)
  
  #Interrupt Due To Sound Sensor
  #To Be Done
  
def collectData():
   
   print('Collecting Sensor Data \n')
     
   #time.sleep(5)
              
   try:
      Collector.collect_sensor()
      #Collector.publish_sensor()
   except OSError as e:
		       print("Cant read:", e)
		       sys.print_exception(e)
             
   #time.sleep(5)
    
client.set_callback(mqttMessages)
client.connect()

client.subscribe("L1Timer", qos=2)
client.subscribe("L2Timer", qos=2)
client.subscribe("L3Timer", qos=2)
client.subscribe("L4Timer", qos=2)

#time.sleep(2)

print('PWLE Application Activated')

#time.sleep(2)
 
while True:
  
   occupiedRoomEvent()
   unoccupiedRoomEvent()





































