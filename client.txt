def occupiedRoomEvent():
  
  #Debug Check  
  if L1 == 6:
     print('L1 Has Been Converted From String To Integer \n')
     print('The Value Of L1 Is ',L1, '\n')
  
  global lastOccupiedEvent
  lastOccupiedEvent = time.time()
  currentEventTime = time.time()
  secSinceCollectData = 0
  
  while True:
  #Debug Check
  print('currentEventTime = ', currentEventTime, '\n')
  print('lastOccupiedEvent = ', lastOccupiedEvent, '\n')
  
  sleepTime = L1 - (currentEventTime - lastOccupiedEvent)
  
  #Debug Check
  print('\n')
  print('sleepTime = ', sleepTime, '\n')
  
  #Determines If Room Is Occupied Or Unoccupied
  #If sleepTime Yields A Result > 0/Positive Value - Room Considered Occupied
  #Unoccupied Will Yield A Result <= 0/Negative Value - Room Considered Unoccupied
  if sleepTime <= 0:
     unoccupiedRoomEvent()
     return #Return To Main Scheduler to move to unoccupiedRoomEvent()
  
  #Sensors Must Be In Sleep Mode Then Wake Up To Take Readings
  print('Room Is Currently Occupied \n')
  print('Going Into Light Sleep Mode \n')
  print('Current Light Sleep Time Is Set To', sleepTime, 'ms \n')
  
  #An Interrupt Means That The ODS Sensors Detected Some Presence In Room
  odsInterrupt()
  
  time.sleep(5)
  
  #Timer
  machine.lightsleep(sleepTime * 1000) #Timer Calculates In ms
  
  #Debug Check
  #If Woken By Interrupt, Value Returned Is 2
  #If Woken By Timer, Value Returned Is 4
  wakeValue = machine.wake_reason()
  print('wakeValue = ', wakeValue, '\n')
  
  if wakeValue == 2:
     print('Light Sleep Interrupted By ODS Sensors \n')
     lastOccupiedEvent = time.time()
     print('Last Occupied Event Is Now', lastOccupiedEvent, '\n')
     occupiedRoomEvent() #This Is Invoked If There Is A New Presence In The Room
     return #Return To Main Execution Body		##PHIL: this return is never executed!
  elif wakeValue == 4:
       print('Light Sleep Ended By Timer \n')
       print('Proceeding To Collect Data Every L3 mins \n')
         
       #Debug Check
       if L3 == 30:
          print('L3 Has Been Converted From String To Integer \n')
          print('The Value Of L3 Is ', L3, '\n')
       else:
           occupiedRoomEvent()
           return
       # PHIL: We simply increment the time since we last woke up
       secSinceCollectData = secSinceCollectData + L1
       print('Time Elapsed = ', secSinceCollectData, 'secs \n')

       if secSinceCollectData >= L3:
         
         secSinceCollectData += 1
         print('Time Elapsed = ', secSinceCollectData, 'secs \n')
         time.sleep(1)
         
         if secSinceCollectData == L3:
            collectData()
            print('\n')
            print('Data Collection For This Period Complete \n')         
 


       
def unoccupiedRoomEvent():
# PHIL: Also needs 
# a loop, rather than recursion, and 
# to return to the SCHEDULER Loop
  
SCHEDULER LOOP

time.sleep(1) #Utilised To Ensure Sensor Stability   
  
  print('PWLE Application Activated \n')

While True:
  client.check_msg()
   occupiedRoomEvent()
   unoccupiedRoomEvent()
  
  
  

 
