#!/usr/bin/env python3
import paho.mqtt.client as mqtt 
import datetime   

def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("#", qos=2)

def on_message(client, userdata, msg):
    receiveTime=datetime.datetime.now()
    message=msg.payload.decode("utf-8")
    isfloatValue=False
    try:
        # Convert the string to a float so that it is stored as a number and not a string in the database
        val = float(message)
        isfloatValue=True
    except:
        isfloatValue=False

    if isfloatValue:
        print(str(receiveTime) + ": " + msg.topic + " " + str(val))
    else:
        print(str(receiveTime) + ": " + msg.topic + " " + message)

# Initialize the client that should connect to the Mosquitto broker
client = mqtt.Client() 
client.on_connect = on_connect 
client.on_message = on_message 
client.connect("IP Address", 1883, 60)

# Blocking loop to the Mosquitto broker
client.loop_forever() 

